import pandas as pd
import numpy as np

df = pd.read_csv("data.csv", skipinitialspace=True)
s = pd.Series(data=[1, 2, 3, 4], index=["one", 2, "3", 4])
print(s)
print()
print(df.head())
