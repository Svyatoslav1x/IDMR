import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from loguru import logger

load_dotenv()

if getattr(sys, "frozen", False):
    ROOT_DIR = Path(sys.executable).parent.absolute()
else:
    ROOT_DIR = Path(__file__).parent.parent.absolute()

FILES_DIR = os.path.join(ROOT_DIR, "files")
ABIS_DIR = os.path.join(ROOT_DIR, "data", "abis")
DEBUG_LOG = os.path.join(FILES_DIR, "debug.log")

logger.add(f"{DEBUG_LOG}", format="{time:YYYY-MM-DD HH:mm:ss} | {level} | {message}", level="DEBUG")

ETHEREUM_API_KEY = str(os.getenv("ETHEREUM_API_KEY"))
ARBITRUM_API_KEY = str(os.getenv("ARBITRUM_API_KEY"))
OPTIMISM_API_KEY = str(os.getenv("OPTIMISM_API_KEY"))
