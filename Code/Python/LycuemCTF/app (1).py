from flask import Flask, Response, request, render_template, redirect, session, url_for
from hashlib import md5
import secrets
from binascii import hexlify, unhexlify

app = Flask(__name__)
# from flag import flag
flag = "success"

admin_password = "ffa55723e25ace95a1942ffba724ced0"

user_passwords = {
    "kek": "kek",
    "yux": "keke",
    "admin": admin_password
}


def get_user(login: str, password: str) -> str:
    if password.lower() == admin_password.lower():
        return "NO"

    for u, p in user_passwords.items():
        if login.upper() == u.upper() and password.upper() == p.upper():
            return u

    return "NO"


@app.route("/", methods=["GET", "POST"])
def _login():
    if request.method == "POST":
        username = request.form.get('username')
        password = request.form.get("password")

        uname = get_user(username, password)

        if uname == "admin":
            return redirect(f"/user/{flag}")

        return redirect(f"/user/{uname}")

    return render_template("login.html")


@app.route("/user/<string:name>")
def _user(name):
    return render_template("user.html", user=name)
