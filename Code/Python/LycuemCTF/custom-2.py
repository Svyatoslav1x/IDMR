﻿def encode_2(inp: str) -> str:
    res = ""
    for s in inp:
        res += bin(ord(s))

    return res


def encode(inp: str) -> str:
    res = ""
    for sym in inp:
        res += encode_2(hex(ord(sym) + 130))

    return res


def decode(inp: str) -> str:
    inp = "".join(list(map(lambda x: chr(int(x, 2)), inp.split("0b")[1:]))).split("0x")
    del inp[0]
    inp = list(map(lambda x: chr(x - 130), list(map(lambda x: int(x, 16), inp))))
    return "".join(inp)


dec = decode(input())
print(dec)
