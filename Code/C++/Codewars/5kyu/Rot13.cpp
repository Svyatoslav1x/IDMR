﻿#include <string>
#include <iostream>
#include <cctype>

std::string rot13(std::string msg) {
    char v;
    bool isUpperCase;
    for (auto& i : msg) {
        if (std::isalpha(i)) {
            v = i;
            isUpperCase = 0;
            if (std::isupper(i)) {
                v += 32;
                isUpperCase = 1;
            }
            if (v > 109) {
                v -= 13;
            }
            else {
                v += 13;
            }
            if (isUpperCase) {
                v -= 32;
            }
            i = v;
        }
    }
    return msg;
}

int main() {
    std::string inpt;
    std::cin >> inpt;
    std::cout << rot13(inpt);
    return 0;
}