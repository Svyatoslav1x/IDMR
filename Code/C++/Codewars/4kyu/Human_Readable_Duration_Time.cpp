﻿#include <string>
#include <vector>
#include <sstream>
#include <iostream>

std::string format_duration(int seconds) {
  if (seconds == 0) return "now";
    int years = seconds / (3600 * 24 * 365);
    int days = seconds % (3600 * 24 * 365) / (3600 * 24);
    int hours = seconds % (3600 * 24 * 365) % (3600 * 24) / 3600;
    int minutes = seconds % (3600 * 24 * 365) % (3600 * 24) % 3600 / 60;
    seconds = seconds % (3600 * 24 * 365) % (3600 * 24) % 3600 % 60;
    std::vector<int> val = {years, days, hours, minutes, seconds};
    std::vector<std::string> cmp = {"years", "days", "hours", "minutes", "seconds"};
    std::stringstream ss;
    int zeroes = 0;
    for (const auto& i: val) {
      if (i != 0)
        zeroes += 1;
    }
    std::string sep = ", ";
    for (int i = 0; i < (int)val.size(); i++) {
        if (val[i] != 0) {
          zeroes--;
          if (zeroes == 1) {
            sep = " and ";
          } else if (zeroes == 0) {
            sep = "";
          }
          if (val[i] > 1) {
              ss << val[i] << " " << cmp[i] << sep;
          } else {
              ss << 1 << " " << cmp[i].substr(0, (int)cmp[i].size() - 1) << sep;
          }
        }
    }
    return ss.str();
}

int main() {
  std::cout << format_duration(132030240);
  return 0;
}