﻿#include <iostream>

class Singleton {
private:
    static Singleton* instance;
    Singleton() {}
public:
    static Singleton* GetInstance() {
        if (instance == nullptr) {
            instance = new Singleton();
        }
        return instance;
    }
    static void DestroyInstance() {
        if (instance == nullptr) {
            return;
        }
        delete instance;
        std::cout << "delete instance";
    }
};

Singleton* Singleton::instance = nullptr;

int main() {
    Singleton* s1 = Singleton::GetInstance();
    Singleton* s2 = Singleton::GetInstance();

    if (s1 == s2) {
        std::cout << "s1 and s2 are the same instance\n";
    }
    s1->DestroyInstance();
}