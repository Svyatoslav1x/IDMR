﻿#include <iostream> 

class A {
public:
    A(int a, int b) {
        std::cout << a + b << "\n";
    }
};

int main() { 
    int* a = new int();
    int* b = new int;
    int* c = new int(10);
    A* d = new A(1, 2);
    std::cout << d << "\n";
    std::cout << a << " " << *a << "\n";
    std::cout << b << " " << *b << "\n";
    std::cout << c << " " << *c << "\n";
    delete a;
    delete b;
    delete c;
} 