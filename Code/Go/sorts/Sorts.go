﻿package sorts

func BubbleSort(list []int) []int {
	for i := 0; i < len(list)-1; i++ {
		for j := 0; j < (len(list) - 1 - i); j++ {
			if list[j] > list[j+1] {
				list[j], list[j+1] = list[j+1], list[j]
			}
		}
	}
	return list
}

func InsertionSort(slice []int) []int {
	for i := 1; i < len(slice); i++ {
		current := slice[i]
		j := i - 1
		for j >= 0 && slice[j] > current {
			slice[j+1] = slice[j]
			j--
		}
		slice[j+1] = current
	}
	return slice
}

func QuickSort(slice []int) []int {
	// Если длина списка меньше 2, он уже отсортирован
	if len(slice) < 2 {
		return slice
	}
	// Выбираем опорный элемент (обычно это первый или последний элемент)
	pivot := slice[0]
	// Создаём пустые списки для элементов меньше и больше опорного элемента
	var less []int
	var greater []int
	// Итерируемся по списку и добавляем элементы в соответствующие списки
	for _, element := range slice[1:] {
		if element <= pivot {
			less = append(less, element)
		} else {
			greater = append(greater, element)
		}
	}
	// Рекурсивно вызываем быструю сортировку для каждого из списков
	// и объединяем результаты вместе с опорным элементом
	return append(append(QuickSort(less), pivot), QuickSort(greater)...)
}

func twoSum(nums []int, target int) []int {
	// Инициализируем два указателя, которые указывают на начало и конец массива
	left, right := 0, len(nums)-1

	// Итерируемся до тех пор, пока указатели не пересекутся или не найдём пару
	for left < right {
		// Вычисляем сумму элементов, которые находятся под указателями left и right
		sum := nums[left] + nums[right]
		// Если сумма равна целевому значению, мы нашли пару и возвращаем её
		if sum == target {
			return []int{nums[left], nums[right]}
			// Если сумма меньше целевого значения, увеличиваем left, чтобы увеличить сумму
		} else if sum < target {
			left++
			// Если сумма больше целевого значения, уменьшаем right, чтобы уменьшить сумму
		} else {
			right--
		}
	}
	// Если пара не найдена, возвращаем nil
	return nil
}
